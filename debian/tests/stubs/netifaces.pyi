import socket

from typing import TypedDict

# Ah well, we can't use NotRequired with Python 3.10, can we now...
class AddressDict(TypedDict):
    addr: str
    broadcast: str
    netmask: str
    peer: str


def interfaces() -> list[str]: ...

def ifaddresses(iface: str) -> dict[socket.AddressFamily, list[AddressDict]]: ...
